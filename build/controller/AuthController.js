"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var config_1 = require("../config/config");
var User_1 = require("../entity/User");
var jwt = require("jsonwebtoken");
var class_validator_1 = require("class-validator");
var rand = require("randexp");
var dateAddMinutes = require("date-fns/add_minutes");
var dateCompareAsc = require("date-fns/compare_asc");
var dateFormat = require("date-fns/format");
var dateAddMonths = require("date-fns/add_months");
var RefreshToken_1 = require("../entity/RefreshToken");
var AuthController = /** @class */ (function () {
    function AuthController() {
    }
    AuthController.prototype.register = function (request, response) {
        return __awaiter(this, void 0, void 0, function () {
            var user, userRepository;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        user = new User_1.User();
                        //Получаем переданные пользователем данные
                        user.firstName = request.body.firstName;
                        user.lastName = request.body.lastName;
                        user.username = request.body.username;
                        user.password = request.body.password;
                        user.email = request.body.email;
                        // TypeORM валидация
                        class_validator_1.validate(user).then(function (errors) {
                            if (errors.length > 0) {
                                return response.status(409).json(errors);
                            }
                            else {
                                // console.log("validation succeed");
                            }
                        });
                        userRepository = typeorm_1.getRepository(User_1.User);
                        return [4 /*yield*/, userRepository.findOne({ email: request.body.email })];
                    case 1:
                        if (!_a.sent()) return [3 /*break*/, 2];
                        response.status(404).json('User with this email exists');
                        return [2 /*return*/];
                    case 2: return [4 /*yield*/, userRepository.findOne({ username: request.body.username })];
                    case 3:
                        if (!_a.sent()) return [3 /*break*/, 4];
                        response.status(404).json('User with this username exists');
                        return [2 /*return*/];
                    case 4:
                        user.hashPassword();
                        // создаём токен
                        // const token = jwt.sign(
                        //     {userId: user.id, username: user.username},
                        //     config.ts.jwtSecret,
                        //     {expiresIn: "1h"}
                        // );
                        // user.token = token;
                        // добавляем нового пользователя в базу
                        return [4 /*yield*/, userRepository.save(user)];
                    case 5:
                        // создаём токен
                        // const token = jwt.sign(
                        //     {userId: user.id, username: user.username},
                        //     config.ts.jwtSecret,
                        //     {expiresIn: "1h"}
                        // );
                        // user.token = token;
                        // добавляем нового пользователя в базу
                        _a.sent();
                        response.status(200).json(user);
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    AuthController.prototype.login = function (request, response) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, username, password, userRepository, user, error_1, accessToken, refreshToken, newRefresh, refreshTokenRepository, err_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = request.body, username = _a.username, password = _a.password;
                        if (!(username && password)) {
                            response.status(400).send();
                            return [2 /*return*/];
                        }
                        userRepository = typeorm_1.getRepository(User_1.User);
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, userRepository.findOneOrFail({ where: { username: username } })];
                    case 2:
                        user = _b.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_1 = _b.sent();
                        response.status(401).json('Incorrect user');
                        return [2 /*return*/];
                    case 4:
                        //Проверяем, корректный ли пароль
                        if (!user.checkIfUnencryptedPasswordIsValid(password)) {
                            response.status(402).send();
                            return [2 /*return*/];
                        }
                        accessToken = this.generateJwt(user);
                        refreshToken = {
                            user: user.id,
                            refreshToken: new rand(/[a-zA-Z0-9_-]{64,64}/).gen(),
                            expiration: dateAddMonths(new Date(), 1),
                            isValid: true
                        };
                        newRefresh = new RefreshToken_1.RefreshToken();
                        newRefresh.user = refreshToken.user;
                        newRefresh.refresh_token = refreshToken.refreshToken;
                        newRefresh.expiration = refreshToken.expiration;
                        newRefresh.isValid = refreshToken.isValid;
                        refreshTokenRepository = typeorm_1.getRepository(RefreshToken_1.RefreshToken);
                        _b.label = 5;
                    case 5:
                        _b.trys.push([5, 7, , 8]);
                        return [4 /*yield*/, refreshTokenRepository.save(newRefresh)];
                    case 6:
                        _b.sent();
                        return [3 /*break*/, 8];
                    case 7:
                        err_1 = _b.sent();
                        return [2 /*return*/, response.status(400).json('error')];
                    case 8:
                        // И отправляем ответ
                        response.status(200);
                        return [2 /*return*/, response.json({ user: user, accessToken: accessToken, refreshToken: refreshToken })];
                }
            });
        });
    };
    ;
    // Когда закончился срок действия access token`а, нам необхожимо получить новый с помощью refresh token`а
    AuthController.prototype.refreshAccessToken = function (request, response) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, user, token, refreshTokenRepository, userRefreshToken, refreshTokenIsValid, error_2, accessToken, refreshToken, newRefresh, err_2;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = request.body, user = _a.user, token = _a.token;
                        if (!user || !token) {
                            return [2 /*return*/, response.status(400).send('Invalid refresh or user')];
                        }
                        refreshTokenRepository = typeorm_1.getRepository(RefreshToken_1.RefreshToken);
                        return [4 /*yield*/, refreshTokenRepository.findOneOrFail({
                                where: {
                                    refresh_token: token.refreshToken
                                }, relations: ['user']
                            })];
                    case 1:
                        userRefreshToken = _b.sent();
                        if (userRefreshToken.user.id !== user.id) {
                            return [2 /*return*/, response.status(400)];
                        }
                        refreshTokenIsValid = dateCompareAsc(dateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'), dateFormat(userRefreshToken.expiration, 'YYYY-MM-DD HH:mm:ss'));
                        if (refreshTokenIsValid !== -1) {
                            return [2 /*return*/, response.status(400).json('Refresh token is expired')];
                        }
                        // старый токен делаем невалидным
                        userRefreshToken.isValid = false;
                        _b.label = 2;
                    case 2:
                        _b.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, refreshTokenRepository.save(userRefreshToken)];
                    case 3:
                        _b.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        error_2 = _b.sent();
                        response.status(401);
                        return [2 /*return*/];
                    case 5:
                        accessToken = this.generateJwt(user);
                        refreshToken = {
                            user: user.id,
                            refreshToken: new rand(/[a-zA-Z0-9_-]{64,64}/).gen(),
                            expiration: dateAddMonths(new Date(), 1),
                            isValid: true
                        };
                        newRefresh = new RefreshToken_1.RefreshToken();
                        newRefresh.user = refreshToken.user;
                        newRefresh.refresh_token = refreshToken.refreshToken;
                        newRefresh.expiration = refreshToken.expiration;
                        newRefresh.isValid = refreshToken.isValid;
                        _b.label = 6;
                    case 6:
                        _b.trys.push([6, 8, , 9]);
                        return [4 /*yield*/, refreshTokenRepository.save(newRefresh)];
                    case 7:
                        _b.sent();
                        return [3 /*break*/, 9];
                    case 8:
                        err_2 = _b.sent();
                        return [2 /*return*/, response.status(400).json('error')];
                    case 9:
                        // И отправляем ответ
                        response.status(200);
                        response.json({ user: user, accessToken: accessToken, refreshToken: refreshToken });
                        return [2 /*return*/];
                }
            });
        });
    };
    ;
    AuthController.prototype.forgot = function (request, response) {
        return __awaiter(this, void 0, void 0, function () {
            var req, resetData, userRepository, user, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        req = request.body;
                        if (!req.email) {
                            response.status(402).json();
                            return [2 /*return*/];
                        }
                        resetData = {
                            passwordResetToken: new rand(/[a-zA-Z0-9_-]{64,64}/).gen(),
                            passwordResetExpiration: dateAddMinutes(new Date(), 30),
                        };
                        userRepository = typeorm_1.getRepository(User_1.User);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, userRepository.findOneOrFail({ where: { email: req.email } })];
                    case 2:
                        user = _a.sent();
                        user.passwordResetExpiration = resetData.passwordResetExpiration;
                        user.passwordResetToken = resetData.passwordResetToken;
                        return [4 /*yield*/, userRepository.save(user)];
                    case 3:
                        _a.sent();
                        response.status(200).json({
                            passwordResetToken: resetData.passwordResetToken,
                            passwordResetExpiration: resetData.passwordResetExpiration
                        });
                        return [3 /*break*/, 5];
                    case 4:
                        error_3 = _a.sent();
                        response.status(400).send('user nor found');
                        return [2 /*return*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    AuthController.prototype.check = function (request, response) {
        return __awaiter(this, void 0, void 0, function () {
            var req, userRepository, user, error_4, tokenIsValid;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        req = request.body;
                        if (!req.passwordResetToken) {
                            response.status(405).json('Incorrect data');
                            return [2 /*return*/];
                        }
                        userRepository = typeorm_1.getRepository(User_1.User);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, userRepository.findOneOrFail({
                                where: {
                                    email: req.email,
                                    passwordResetToken: req.passwordResetToken
                                }
                            })];
                    case 2:
                        user = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_4 = _a.sent();
                        response.status(401).json('user nor found');
                        return [2 /*return*/];
                    case 4:
                        if (!user.passwordResetExpiration) {
                            response.status(402).json('Invalid token');
                            return [2 /*return*/];
                        }
                        tokenIsValid = dateCompareAsc(dateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'), dateFormat(user.passwordResetExpiration, 'YYYY-MM-DD HH:mm:ss'));
                        if (tokenIsValid !== -1) {
                            response.status(403).json('Invalid data');
                            return [2 /*return*/];
                        }
                        response.status(200).json({ message: 'success' });
                        return [2 /*return*/];
                }
            });
        });
    };
    AuthController.prototype.reset = function (request, response) {
        return __awaiter(this, void 0, void 0, function () {
            var req, userData, userRepository, user, error_5, tokenIsValid, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        req = request.body;
                        userData = new User_1.User();
                        userData.email = request.body;
                        userData.passwordResetToken = request.body;
                        userData.password = request.body.password;
                        userRepository = typeorm_1.getRepository(User_1.User);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, userRepository.findOneOrFail({ where: { email: request.body.email } })];
                    case 2:
                        user = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        error_5 = _a.sent();
                        response.status(400).send('user nor found');
                        return [2 /*return*/];
                    case 4:
                        if (!user.passwordResetExpiration) {
                            response.status(401).json('Incorrect data');
                            return [2 /*return*/];
                        }
                        tokenIsValid = dateCompareAsc(dateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'), dateFormat(user.passwordResetExpiration, 'YYYY-MM-DD HH:mm:ss'));
                        if (tokenIsValid !== -1) {
                            response.status(402).json('Incorrect data');
                            return [2 /*return*/];
                        }
                        //убираем из базы токен
                        userData.passwordResetToken = null;
                        userData.passwordResetExpiration = null;
                        user.passwordResetToken = userData.passwordResetToken;
                        user.passwordResetExpiration = userData.passwordResetExpiration;
                        user.password = userData.password;
                        user.hashPassword();
                        _a.label = 5;
                    case 5:
                        _a.trys.push([5, 7, , 8]);
                        return [4 /*yield*/, userRepository.save(user)];
                    case 6:
                        _a.sent();
                        response.status(200).json(user);
                        return [3 /*break*/, 8];
                    case 7:
                        err_3 = _a.sent();
                        response.status(408).json(err_3);
                        return [2 /*return*/];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    AuthController.prototype.generateJwt = function (user) {
        return __awaiter(this, void 0, void 0, function () {
            var accessToken;
            return __generator(this, function (_a) {
                accessToken = jwt.sign({ userId: user.id, username: user.username }, config_1.default.jwtOpts.secret, { expiresIn: "1h" });
                return [2 /*return*/, accessToken];
            });
        });
    };
    return AuthController;
}());
exports.default = new AuthController();
//# sourceMappingURL=AuthController.js.map