"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var Note_1 = require("../entity/Note");
var class_validator_1 = require("class-validator");
var User_1 = require("../entity/User");
var NoteController = /** @class */ (function () {
    function NoteController() {
    }
    NoteController.all = function (request, response, next) { return __awaiter(_this, void 0, void 0, function () {
        var notes;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, typeorm_1.getRepository(Note_1.Note).find()];
                case 1:
                    notes = _a.sent();
                    response.send(notes);
                    return [2 /*return*/];
            }
        });
    }); };
    NoteController.one = function (request, response, next) { return __awaiter(_this, void 0, void 0, function () {
        var errors, note, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, class_validator_1.validate(request.body)];
                case 1:
                    errors = _a.sent();
                    if (errors.length > 0) {
                        response.status(400).send(errors);
                        return [2 /*return*/];
                    }
                    _a.label = 2;
                case 2:
                    _a.trys.push([2, 4, , 5]);
                    return [4 /*yield*/, typeorm_1.getRepository(Note_1.Note).findOneOrFail({ where: { id: request.params.id }, relations: ['user'] })];
                case 3:
                    note = _a.sent();
                    response.send(note);
                    return [3 /*break*/, 5];
                case 4:
                    error_1 = _a.sent();
                    return [2 /*return*/, response.status(404).json('note not found')];
                case 5: return [2 /*return*/];
            }
        });
    }); };
    NoteController.save = function (request, response, next) { return __awaiter(_this, void 0, void 0, function () {
        var note, noteRepository, e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    note = new Note_1.Note();
                    note.title = request.body.title;
                    note.content = request.body.content;
                    note.user = request.logged_user;
                    class_validator_1.validate(note).then(function (errors) {
                        if (errors.length > 0) {
                            return response.status(409).json(errors);
                        }
                        else {
                            console.log("validation succeed");
                        }
                    });
                    noteRepository = typeorm_1.getRepository(Note_1.Note);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, noteRepository.save(note)];
                case 2: return [2 /*return*/, _a.sent()];
                case 3:
                    e_1 = _a.sent();
                    response.status(408).send("notename already in use");
                    return [2 /*return*/];
                case 4: return [2 /*return*/];
            }
        });
    }); };
    NoteController.edit = function (req, response) { return __awaiter(_this, void 0, void 0, function () {
        var id, _a, title, content, user_id, noteRepository, note, error_2, userRepository, user, err_1, errors, newNote, e_2;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    id = req.params.id;
                    _a = req.body, title = _a.title, content = _a.content;
                    user_id = req.logged_user;
                    noteRepository = typeorm_1.getRepository(Note_1.Note);
                    _b.label = 1;
                case 1:
                    _b.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, noteRepository.findOneOrFail(id)];
                case 2:
                    note = _b.sent();
                    return [3 /*break*/, 4];
                case 3:
                    error_2 = _b.sent();
                    response.status(404).send("Note not found");
                    return [2 /*return*/];
                case 4:
                    userRepository = typeorm_1.getRepository(User_1.User);
                    _b.label = 5;
                case 5:
                    _b.trys.push([5, 7, , 8]);
                    return [4 /*yield*/, userRepository.findOneOrFail(user_id)];
                case 6:
                    user = _b.sent();
                    return [3 /*break*/, 8];
                case 7:
                    err_1 = _b.sent();
                    response.status(503).json('user not found');
                    return [2 /*return*/];
                case 8:
                    if (user.id !== note.user_id) {
                        response.status(503).json('permissions denied');
                        return [2 /*return*/];
                    }
                    note.title = title;
                    note.content = content;
                    return [4 /*yield*/, class_validator_1.validate(note)];
                case 9:
                    errors = _b.sent();
                    if (errors.length > 0) {
                        response.status(400).send(errors);
                        return [2 /*return*/];
                    }
                    _b.label = 10;
                case 10:
                    _b.trys.push([10, 12, , 13]);
                    return [4 /*yield*/, noteRepository.save(note)];
                case 11:
                    newNote = _b.sent();
                    return [3 /*break*/, 13];
                case 12:
                    e_2 = _b.sent();
                    response.status(409).send("error");
                    return [2 /*return*/];
                case 13:
                    response.status(200).send(newNote);
                    return [2 /*return*/];
            }
        });
    }); };
    NoteController.remove = function (request, response, next) { return __awaiter(_this, void 0, void 0, function () {
        var id, user_id, noteRepository, note, error_3, userRepository, user, err_2, err_3;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    id = request.params.id;
                    user_id = request.logged_user;
                    noteRepository = typeorm_1.getRepository(Note_1.Note);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, noteRepository.findOneOrFail(id)];
                case 2:
                    note = _a.sent();
                    return [3 /*break*/, 4];
                case 3:
                    error_3 = _a.sent();
                    response.status(404).send("Note not found");
                    return [2 /*return*/];
                case 4:
                    userRepository = typeorm_1.getRepository(User_1.User);
                    _a.label = 5;
                case 5:
                    _a.trys.push([5, 7, , 8]);
                    return [4 /*yield*/, userRepository.findOneOrFail(user_id)];
                case 6:
                    user = _a.sent();
                    return [3 /*break*/, 8];
                case 7:
                    err_2 = _a.sent();
                    response.status(503).json('user not found');
                    return [2 /*return*/];
                case 8:
                    if (user.id !== note.user_id) {
                        response.status(503).json('permissions denied');
                        return [2 /*return*/];
                    }
                    _a.label = 9;
                case 9:
                    _a.trys.push([9, 11, , 12]);
                    return [4 /*yield*/, noteRepository.delete(id)];
                case 10:
                    _a.sent();
                    return [3 /*break*/, 12];
                case 11:
                    err_3 = _a.sent();
                    return [2 /*return*/, response.status(400).json('something went wrong')];
                case 12:
                    response.status(204).send();
                    return [2 /*return*/];
            }
        });
    }); };
    NoteController.deleteAll = function (request, response, next) { return __awaiter(_this, void 0, void 0, function () {
        var noteRepository;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    noteRepository = typeorm_1.getRepository(Note_1.Note);
                    return [4 /*yield*/, noteRepository.clear()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); };
    return NoteController;
}());
exports.default = NoteController;
//# sourceMappingURL=NoteController.js.map