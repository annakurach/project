"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var server = require("./index");
var config_1 = require("./config/config");
server.listen(Number(process.env.SERVER_PORT), function () {
    console.log(config_1.default);
    console.log("Server started on port " + process.env.SERVER_PORT);
});
//# sourceMappingURL=server.js.map