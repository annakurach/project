"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var UserController_1 = require("../controller/UserController");
var router = express_1.Router();
router.get("/", UserController_1.default.all);
router.get("/:id([0-9]+)", UserController_1.default.one);
router.post("/", UserController_1.default.save);
//Edit one user
router.patch("/:id([0-9]+)", UserController_1.default.edit);
//Delete one user
router.delete("/:id([0-9]+)", UserController_1.default.remove);
router.delete("/deleteAll", UserController_1.default.deleteAll);
exports.default = router;
//# sourceMappingURL=user.js.map