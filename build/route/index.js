"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var user_1 = require("./user");
var note_1 = require("./note");
var auth_1 = require("./auth");
var routes = express_1.Router();
routes.use("/user", user_1.default);
routes.use("/note", note_1.default);
routes.use("/auth", auth_1.default);
exports.default = routes;
//# sourceMappingURL=index.js.map