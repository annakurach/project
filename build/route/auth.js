"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var AuthController_1 = require("../controller/AuthController");
var router = express_1.Router();
router.post("/refreshAccessToken", function (req, res) { return AuthController_1.default.refreshAccessToken(req, res); });
router.post("/forgot", function (req, res) { return AuthController_1.default.forgot(req, res); });
router.post("/check", function (req, res) { return AuthController_1.default.check(req, res); });
router.post("/reset", function (req, res) { return AuthController_1.default.reset(req, res); });
router.post("/login", function (req, res) { return AuthController_1.default.login(req, res); });
router.post("/register", function (req, res) { return AuthController_1.default.register(req, res); });
exports.default = router;
//# sourceMappingURL=auth.js.map