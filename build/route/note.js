"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var NoteController_1 = require("../controller/NoteController");
var jwt_1 = require("../middleware/jwt");
var router = express_1.Router();
router.get("/", NoteController_1.default.all);
router.get("/:id([0-9]+)", jwt_1.default.checkToken, NoteController_1.default.one);
router.post("/", jwt_1.default.checkToken, NoteController_1.default.save);
//Edit one user
router.patch("/:id([0-9]+)", jwt_1.default.checkToken, NoteController_1.default.edit);
//Delete one user
router.delete("/:id([0-9]+)", jwt_1.default.checkToken, NoteController_1.default.remove);
router.delete("/deleteAll", jwt_1.default.checkToken, NoteController_1.default.deleteAll);
exports.default = router;
//# sourceMappingURL=note.js.map