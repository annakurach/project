"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app = require("../index");
var supertest = require("supertest");
var date_fns_1 = require("date-fns");
before(function () {
    supertest(app)
        .post('/user/deleteAll');
});
var date = date_fns_1.addMinutes(new Date(), 30);
describe('POST /auth/register', function () {
    var data = {
        "username": "user1",
        "firstName": "user1",
        "lastName": "user1",
        "password": "user1",
        "email": "user1",
        "passwordResetToken": "qwwertyuiopp[asdfghjklzxcvbnm",
        "passwordResetExpiration": date
    };
    it('respond with 200, create user', function (done) {
        supertest(app)
            .post('/auth/register')
            .send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err) {
            if (err)
                return done(err);
            done();
        });
    });
});
describe('POST /auth/register', function () {
    var data = {
        "username": "user2",
        "firstName": "user2",
        "lastName": "user2",
        "password": "user2",
        "email": "user2",
        "passwordResetToken": "qwwertyuiopp[asdfghjklzxcvbnm",
        "passwordResetExpiration": date
    };
    before(function (done) {
        supertest(app)
            .post('/auth/register')
            .send(data)
            .end(function (err) {
            if (err)
                return done(err);
            done();
        });
    });
    it('respond with 400, not create user, duplicate', function (done) {
        supertest(app)
            .post('/auth/register')
            .send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(404)
            .end(function (err) {
            if (err)
                return done(err);
            done();
        });
    });
});
describe('POST /auth/login', function () {
    var wrong = {
        "username": "dummy75555",
        "password": "12345678"
    };
    it('respond with 402', function (done) {
        supertest(app)
            .post('/auth/login')
            .send(wrong)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(401)
            .end(function (err) {
            if (err)
                return done(err);
            done();
        });
    });
});
// describe('GET /user/:id', function () {
//     it('respond with json containing a single user', function (done) {
//         supertest(app)
//             .get('/user/1')
//             .set('Accept', 'application/json')
//             .expect('Content-Type', /json/)
//             .expect(200)
//             .end((err) => {
//                 if (err) return done(err);
//                 done();
//             });
//     });
// });
//
// describe('GET /user/:id', function () {
//     it('respond with json user not found', function (done) {
//         supertest(app)
//             .get('/user/43523456345')
//             .set('Accept', 'application/json')
//             .expect(404)
//             .expect('"user not found"')
//             .end((err) => {
//                 if (err) return done(err);
//                 done();
//             });
//     });
// });
//
// describe('GET /user', function () {
//     it('responds with json containing list of all users', function () {
//         return supertest(app)
//             .get('/user')
//             .set('Accept', 'application/json')
//             .expect('Content-Type', /json/)
//             .expect(200);
//
//     });
// });
var passwordResetToken;
var token;
describe('POST /auth/forgotPassword', function () {
    var data = {
        lastName: 'asdasfd',
        firstName: 'sdfsdf',
        username: 'dfsdfsdf',
        email: '12345678',
        password: '12345678',
        passwordResetToken: "qwwertyuiopp[asdfghjklzxcvbnm",
        passwordResetExpiration: date
    };
    before(function (done) {
        supertest(app)
            .post('/auth/register')
            .send(data)
            .then(function (res, err) {
            if (err)
                return done(err);
            done();
        });
    });
    var forgotData = {
        "email": '12345678',
        "url": 'localhost/user/reset',
        "type": 'web'
    };
    it('respond with 200 containing token', function (done) {
        supertest(app)
            .post('/auth/forgot')
            .send(forgotData)
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
            //res.body.should.have.property('passwordResetToken');
            if (err)
                return done(err);
            done();
        });
    });
});
var checkData = {
    "email": "8765432176",
    "passwordResetToken": "qwwertyuiopp[asdfghjklzxcvbnm"
};
var refreshData;
describe('POST /auth/reset', function () {
    var data = {
        "username": "8765432176",
        "firstName": "8765432176",
        "lastName": "8765432176",
        "password": "8765432176",
        "email": "8765432176",
        "passwordResetToken": "qwwertyuiopp[asdfghjklzxcvbnm",
        "passwordResetExpiration": date
    };
    before(function (done) {
        supertest(app)
            .post('/auth/register')
            .send(data)
            .then(function (res, err) {
            if (err)
                return done(err);
            done();
        });
    });
    it('respond with 200, check token', function (done) {
        supertest(app)
            .post('/auth/check')
            .send(checkData)
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err) {
            if (err)
                return done(err);
            done();
        });
    });
});
var user, accessToken, refreshToken;
describe('POST /auth/reset', function () {
    var resetData = {
        email: '8765432176',
        passwordResetToken: checkData.passwordResetToken,
        password: 'new12345678',
    };
    it('respond with 200, reset password', function (done) {
        console.log(resetData);
        supertest(app)
            .post('/auth/reset')
            .send(resetData)
            .expect('Content-Type', /json/)
            .expect(200)
            .then(function (res, err) {
            if (err)
                return done(err);
            done();
        });
    });
});
describe('POST /auth/reset', function () {
    var userData = {
        lastName: 'asdasfdsaf',
        firstName: 'sdfsdffsdfs',
        username: 'dfsdfsdffsdfsd',
        email: '12345678fsdfs',
        password: '12345678fsdfsd',
        passwordResetToken: "qwwertyuiopp[asdfghjklzxcvbnm",
        passwordResetExpiration: date
    };
    before(function (done) {
        supertest(app)
            .post('/auth/register')
            .send(userData)
            .then(function (res, err) {
            if (err)
                return done(err);
            done();
        });
    });
    it('respond with 200 containing two token', function (done) {
        var user = {
            username: 'dfsdfsdffsdfsd',
            password: '12345678fsdfsd'
        };
        supertest(app)
            .post('/user/login')
            .send(user)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (err, res) {
            console.log(res.body);
            accessToken = res.body.accessToken;
            refreshToken = res.body.refreshToken;
            refreshData = {
                user: res.body,
                token: refreshToken
            };
            if (err)
                return done(err);
            done();
        });
    });
    it('respond with 200 containing new pairs of tokens', function (done) {
        supertest(app)
            .post('/auth/refreshAccessToken')
            .send(refreshData)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end(function (res, err) {
            if (err)
                return done(err);
            done();
        });
    });
});
//# sourceMappingURL=auth.js.map