"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
var app = require("../index");
var supertest = require("supertest");
var date_fns_1 = require("date-fns");
var user_id;
var date = date_fns_1.addMinutes(new Date(), 30);
before(function (done) {
    supertest(app)
        .delete('/note/deleteAll')
        .then(function (res, err) {
        if (err)
            return done(err);
        done();
    });
    supertest(app)
        .delete('/user/deleteAll')
        .then(function (res, err) {
        if (err)
            return done(err);
        done();
    });
});
describe('POST /note', function () {
    var userData = {
        "username": "1234567856",
        "firstName": "12345678745",
        "lastName": "1234567835",
        "password": "645654",
        "email": "12345678645",
    };
    var login = {
        "username": "1234567856",
        "password": "645654",
    };
    before(function (done) {
        supertest(app)
            .post('/auth/register')
            .send(userData)
            .then(function (res, err) {
            if (err)
                return done(err);
            done();
        });
    });
    var token;
    before(function (done) {
        supertest(app)
            .post('/auth/login')
            .send(login)
            .then(function (res, err) {
            token = res.body.accessToken;
            if (err)
                return done(err);
            done();
        });
    });
    var noteData = {
        "title": "12345678",
        "content": "12345678",
        "user_id": "1",
    };
    it('add new note', function (done) {
        supertest(app)
            .post('/note')
            .send(noteData) // x-www-form-urlencoded upload
            .set('Accept', 'application/json')
            .set('x-access-token', token)
            .expect(200, done);
    });
});
describe('PATCH /note', function () {
    var userData = {
        "username": "1234567856",
        "firstName": "12345678745",
        "lastName": "1234567835",
        "password": "645654",
        "email": "12345678645",
    };
    var login = {
        "username": "1234567856",
        "password": "645654",
    };
    before(function (done) {
        supertest(app)
            .post('/auth/register')
            .send(userData)
            .then(function (res, err) {
            if (err)
                return done(err);
            done();
        });
    });
    before(function (done) {
        supertest(app)
            .post('/auth/login')
            .send(login)
            .then(function (res, err) {
            if (err)
                return done(err);
            done();
        });
    });
    var noteData = {
        "title": "12345678"
    };
    it('add new note', function (done) {
        supertest(app)
            .post('/note')
            .send(noteData) // x-www-form-urlencoded upload
            .set('Accept', 'application/json')
            .expect(200, done);
    });
});
//# sourceMappingURL=note.js.map