"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var dotenv = require("dotenv");
var path_1 = require("path");
if (process.env.NODE_ENV) {
    process.env.NODE_ENV
        .split(',')
        .filter(function (val) { return val; })
        .forEach(function (envPath) {
        dotenv.config({ path: __dirname + "/../../.env." + envPath });
    });
}
var parentDir = path_1.join(__dirname, '..');
var connectionOpts = {
    type: 'postgres',
    host: process.env.TYPEORM_HOST,
    port: Number(process.env.TYPEORM_PORT),
    username: process.env.TYPEORM_USERNAME,
    password: process.env.TYPEORM_PASSWORD,
    database: process.env.TYPEORM_DATABASE,
    entities: [
        parentDir + "/entity/*.ts",
    ],
    synchronize: true,
};
var jwtOpts = {
    secret: process.env.JWT_SECRET
};
var serverOpts = {
    port: process.env.SERVER_PORT || 3000
};
var options = {
    connectionOpts: connectionOpts,
    jwtOpts: jwtOpts,
    serverOpts: serverOpts
};
console.log(options);
exports.default = options;
//# sourceMappingURL=config.js.map