"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var log4js_1 = require("log4js");
// configure('./main.log');
log4js_1.configure({
    appenders: {
        file: {
            type: 'file',
            filename: 'logs/main.log',
            maxLogSize: 20480,
            backups: 10,
        },
        console: {
            type: 'stdout',
        },
    },
    categories: {
        development: {
            appenders: ['file', 'console'],
            level: 'all',
        },
        production: {
            appenders: ['file'],
            level: 'info',
        },
        default: {
            appenders: ['file'],
            level: 'info',
        },
    },
});
var logger = process.env.NODE_ENV === 'development'
    ? log4js_1.getLogger('development')
    : log4js_1.getLogger('production');
exports.default = logger;
//# sourceMappingURL=log.js.map