import {Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn} from "typeorm";
import {Note} from "./Note";
import * as bcrypt from "bcryptjs";
import {Length} from "class-validator";
import {RefreshToken} from "./RefreshToken";
@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @Length(2, 20)
    firstName: string;

    @Column()
    @Length(2, 20)
    lastName: string;

    @Column()
    @Length(2, 20)
    username: string;

    @Column()
    @Length(2, 20)
    email: string;

    @Column()
    @Length(2, 20)
    password: string;

    // @Column()
    // token: string;

    @Column({ nullable: true })
    passwordResetToken: string;

    @Column({ nullable: true })
    passwordResetExpiration: Date;

    @OneToMany(type => Note, note => note.user)
    notes: Note[];

    // @OneToMany(type => RefreshToken, refreshToken =)
    // @JoinColumn()
    // refreshToken: RefreshToken;


    @OneToMany(type => RefreshToken, refreshToken => refreshToken.user)
    tokens: RefreshToken[];

    hashPassword() {
        this.password = bcrypt.hashSync(this.password, 8);
    }

    checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
        return bcrypt.compareSync(unencryptedPassword, this.password);
    }


}
