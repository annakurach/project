import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToOne, JoinColumn, ObjectID, ManyToOne
} from "typeorm";
import {User} from "./User";


@Entity()
export class RefreshToken{

    @PrimaryGeneratedColumn("increment", { type: "bigint" })
    id: string;

    @Column('text')
    refresh_token: string;

    @Column()
    isValid: boolean;

    @Column()
    expiration: Date;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(type => User, user => user.tokens)
    user: User;


}
