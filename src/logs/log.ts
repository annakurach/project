import { configure, getLogger } from 'log4js';
// configure('./main.log');
configure({
    appenders: {
        file: {
            type: 'file',
            filename: 'logs/main.log',
            maxLogSize: 20480,
            backups: 10,
        },
        console: {
            type: 'stdout',
        },
    },
    categories: {
        development: {
            appenders: ['file', 'console'],
            level: 'all',
        },
        production: {
            appenders: ['file'],
            level: 'info',
        },
        default: {
            appenders: ['file'],
            level: 'info',
        },
    },
})
const logger =
    process.env.NODE_ENV === 'development'
        ? getLogger('development')
        : getLogger('production')

export default logger
