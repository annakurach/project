import { Router } from "express";
import UserController from "../controller/UserController";
const router = Router();

router.get("/",  UserController.all);

router.get(
    "/:id([0-9]+)",
    UserController.one
);

router.post("/", UserController.save);

//Edit one user
router.patch(
    "/:id([0-9]+)",
    UserController.edit
);

//Delete one user
router.delete(
    "/:id([0-9]+)",
    UserController.remove
);

router.delete(
    "/deleteAll",
    UserController.deleteAll
);


export default router;
