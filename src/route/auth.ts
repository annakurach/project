import {Router} from "express";
import AuthController from "../controller/AuthController";
import {Request, Response} from "express";

const router = Router();
router.post("/refreshAccessToken", (req: Request, res: Response) => AuthController.refreshAccessToken(req, res));
router.post("/forgot", (req: Request, res: Response) => AuthController.forgot(req, res));
router.post("/check", (req: Request, res: Response) => AuthController.check(req, res));
router.post("/reset", (req: Request, res: Response) => AuthController.reset(req, res));
router.post("/login", (req: Request, res: Response) => AuthController.login(req, res));
router.post("/register", (req: Request, res: Response) => AuthController.register(req, res));


export default router;
