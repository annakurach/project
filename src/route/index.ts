import { Router, Request, Response } from "express";
import user from "./user";
import note from "./note";
import auth from "./auth";
const routes = Router();
routes.use("/user", user);
routes.use("/note", note);
routes.use("/auth", auth);
export default routes;
