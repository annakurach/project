import { Router } from "express";
import NoteController from "../controller/NoteController";
import jwtoken from "../middleware/jwt";
const router = Router();

router.get(
    "/",
    NoteController.all);

router.get(
    "/:id([0-9]+)",jwtoken.checkToken,
    NoteController.one
);

router.post(
    "/",
    jwtoken.checkToken,
    NoteController.save);

//Edit one user
router.patch(
    "/:id([0-9]+)",
    jwtoken.checkToken,
    NoteController.edit
);

//Delete one user
router.delete(
    "/:id([0-9]+)",
    jwtoken.checkToken,
    NoteController.remove
);

router.delete(
    "/deleteAll",
    jwtoken.checkToken,
    NoteController.deleteAll
);


export default router;
