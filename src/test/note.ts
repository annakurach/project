// process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
import * as app from "../index";
import * as supertest from "supertest";
import {addMinutes} from "date-fns";

let user_id;
let date = addMinutes(new Date(), 30);
before(function (done) {
    supertest(app)
        .delete('/note/deleteAll')
        .then((res, err) => {
            if (err) return done(err);
            done();
        });

    supertest(app)
        .delete('/user/deleteAll')
        .then((res, err) => {
            if (err) return done(err);
            done();
        });
});
describe('POST /note', function () {

    let userData = {
        "username": "1234567856",
        "firstName": "12345678745",
        "lastName": "1234567835",
        "password": "645654",
        "email": "12345678645",
    };

    let login = {
        "username": "1234567856",
        "password": "645654",
    };
    before(function (done) {
        supertest(app)
            .post('/auth/register')
            .send(userData)
            .then((res, err) => {
                if (err) return done(err);
                done();
            });
    });
    let token ;
    before(function (done) {
        supertest(app)
            .post('/auth/login')
            .send(login)
            .then((res, err) => {
                token = res.body.accessToken;
                if (err) return done(err);
                done();
            });
    });
    let noteData = {
        "title": "12345678",
        "content": "12345678",
        "user_id": "1",
    };
    it('add new note', function (done) {
        supertest(app)
            .post('/note')
            .send(noteData) // x-www-form-urlencoded upload
            .set('Accept', 'application/json')
            .set('x-access-token',token)
            .expect(200, done);
    });
});

describe('PATCH /note', function () {
    let userData = {
        "username": "1234567856",
        "firstName": "12345678745",
        "lastName": "1234567835",
        "password": "645654",
        "email": "12345678645",
    };

    let login = {
        "username": "1234567856",
        "password": "645654",
    };
    before(function (done) {
        supertest(app)
            .post('/auth/register')
            .send(userData)
            .then((res, err) => {
                if (err) return done(err);
                done();
            });
    });
    before(function (done) {
        supertest(app)
            .post('/auth/login')
            .send(login)
            .then((res, err) => {
                if (err) return done(err);
                done();
            });
    });
    let noteData = {
        "title": "12345678"
    };
    it('add new note', function (done) {
        supertest(app)
            .post('/note')
            .send(noteData) // x-www-form-urlencoded upload
            .set('Accept', 'application/json')
            .expect(200, done);
    });
});

