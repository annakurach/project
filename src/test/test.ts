process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
import * as app from "../index";
import * as supertest from "supertest";
import {addMinutes} from "date-fns";


before(function (done) {
    supertest(app)
        .delete('/note/deleteAll')
        .then((res, err) => {
            if (err) return done(err);
            done();
        });

    supertest(app)
        .delete('/user/deleteAll')
        .then((res, err) => {
            if (err) return done(err);
            done();
        });
});
let date = addMinutes(new Date(), 30);

describe('POST /auth/register', function () {

    let data = {
        "username": "username",
        "firstName": "firstName",
        "lastName": "lastName",
        "password": "password",
        "email": "email"
    };
    it('create new user', function (done) {
        supertest(app)
            .post('/auth/register')
            .send(data)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
});

let user, accessToken, refreshToken, passwordResetExpiration, passwordResetToken, refreshData;
describe('POST /auth/login', function () {

    let data = {
        "username": "username1",
        "firstName": "firstName1",
        "lastName": "lastName1",
        "password": "password1",
        "email": "email1"
    };

    let user = {
        username: data.username,
        password: data.password
    };


    before(function (done) {
        supertest(app)
            .post('/auth/register')
            .send(data)
            .then((res, err) => {
                if (err) return done(err);
                done();
            });
    });
    it('login user', function (done) {
        supertest(app)
            .post('/auth/login')
            .send(user)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                accessToken = res.body.accessToken;
                refreshToken = res.body.refreshToken;
                refreshData = {
                    user: res.body.user,
                    token: refreshToken
                };
                if (err) return done(err);
                console.log(res);
                done();
            });
    });

    it('respond with 200 containing new pairs of tokens', function (done) {
        supertest(app)
            .post('/auth/refreshAccessToken')
            .send(refreshData)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                console.log(res);
                done();
            });
    });
});
let newUser = {
    lastName: 'asdasfd',
    firstName: 'sdfsdf',
    username: 'dfsdfsdf',
    email: '12345678',
    password: '12345678'
};
describe('POST /auth/forgotPassword', function () {


    before(function (done) {
        supertest(app)
            .post('/auth/register')
            .send(newUser)
            .then((res, err) => {
                if (err) return done(err);
                user = res.body.user;
                done();
            });
    });
    let forgotData = {
        "email": newUser.email,
        "url": 'localhost/user/reset',
        "type": 'web'
    };


    it('respond with 200 containing token', function (done) {
        supertest(app)
            .post('/auth/forgot')
            .send(forgotData)
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                passwordResetToken = res.body.passwordResetToken;
                passwordResetExpiration = res.body.passwordResetExpiration;
                done();
            });
    });

    it('respond with 200, check token', function (done) {
        let checkData = {
            email: newUser.email,
            passwordResetToken: passwordResetToken
        };
        supertest(app)
            .post('/auth/check')
            .send(checkData)
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err) => {
                if (err) return done(err);
                done();
            });
    });
    let resetData = {
        email: newUser.email,
        passwordResetToken: passwordResetToken,
        password: 'new12345678',
    };
    it('respond with 200, reset password', function (done) {
        supertest(app)
            .post('/auth/reset')
            .send(resetData)
            .expect('Content-Type', /json/)
            .expect(200)
            .then((res, err) => {
                if (err) return done(err);
                done();
            });
    });
});



