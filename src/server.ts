let server = require("./index");
import options from "./config/config";
server.listen(Number(process.env.SERVER_PORT), () => {
    console.log("Server started on port "+process.env.SERVER_PORT);
});


