import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import options from "./config/config";
import routes from "./route/index"

const app = express();
app.use(bodyParser.json());

// это то, о чём мы говорили - как вызвать логирование в самом конце
app.use(async (req, res, next) => {
    let t1 = Date.now();
    await next();
    let t2 = Date.now();
    // logger.info('Request executed in ' + Math.ceil(t2 - t1) + 'ms');
});
app.use("/", routes);
// app.use(jwt.checkToken);

module.exports = app;
createConnection(options.connectionOpts)
    .then(async () => {

    })
    .catch(error => console.log(error));
