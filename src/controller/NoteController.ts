import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Note} from "../entity/Note";
import {validate} from "class-validator";
import {User} from "../entity/User";

class NoteController {

    static all = async (request: Request, response: Response, next: NextFunction) => {
        let notes = await getRepository(Note).find();
        response.send(notes);
    }

    static one = async (request: Request, response: Response, next: NextFunction) => {
        const errors = await validate(request.body);
        if (errors.length > 0) {
            response.status(400).send(errors);
            return;
        }
        let note;
        try {
            note = await getRepository(Note).findOneOrFail({where: {id: request.params.id}, relations: ['user']});
            response.send(note);
        } catch (error) {
            return response.status(404).json('note not found');
        }
    }

    static save = async (request: Request, response: Response, next: NextFunction) => {
        let note = new Note();
        note.title = request.body.title;
        note.content = request.body.content;
        note.user = request.logged_user;
        validate(note).then(errors => {
                if (errors.length > 0) {
                    return response.status(409).json(errors);
                } else {
                    console.log("validation succeed");
                }
            }
        );
        const noteRepository = getRepository(Note);
        try {
            return await noteRepository.save(note);
        } catch (e) {
            response.status(408).send("notename already in use");
            return;
        }
    }

    static edit = async (req: Request, response: Response) => {
        const id = req.params.id;
        const {title, content} = req.body;
        const user_id = req.logged_user;
        const noteRepository = getRepository(Note);
        let note;
        try {
            note = await noteRepository.findOneOrFail(id);
        } catch (error) {
            response.status(404).send("Note not found");
            return;
        }
        const userRepository = getRepository(User);
        let user;
        try {
            user = await userRepository.findOneOrFail(user_id);
        } catch (err) {
            response.status(503).json('user not found');
            return;
        }
        if (user.id !== note.user_id) {
            response.status(503).json('permissions denied');
            return;
        }
        note.title = title;
        note.content = content;
        const errors = await validate(note);
        if (errors.length > 0) {
            response.status(400).send(errors);
            return;
        }
        let newNote;
        try {
            newNote = await noteRepository.save(note);
        } catch (e) {
            response.status(409).send("error");
            return;
        }
        response.status(200).send(newNote);
    };

    static remove = async (request: Request, response: Response, next: NextFunction) => {
        //Get the ID from the url
        const id = request.params.id;
        const user_id = request.logged_user;
        const noteRepository = getRepository(Note);
        let note;
        try {
            note = await noteRepository.findOneOrFail(id);
        } catch (error) {
            response.status(404).send("Note not found");
            return;
        }
        const userRepository = getRepository(User);
        let user;
        try {
            user = await userRepository.findOneOrFail(user_id);
        } catch (err) {
            response.status(503).json('user not found');
            return;
        }
        if (user.id !== note.user_id) {
            response.status(503).json('permissions denied');
            return;
        }
        try{
            await noteRepository.delete(id);
        }catch(err){
            return response.status(400).json('something went wrong');
        }

        response.status(204).send();
    }

    static deleteAll = async (request: Request, response: Response, next: NextFunction) => {
        const noteRepository = getRepository(Note);
        await noteRepository.clear();
    }

}


export default NoteController;
