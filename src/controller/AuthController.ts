import {getRepository} from "typeorm";
import options from "../config/config";
import {NextFunction, Request, Response} from "express";
import {User} from "../entity/User";
import * as jwt from "jsonwebtoken";
import {validate} from "class-validator";
import * as rand from 'randexp'
import * as dateAddMinutes from 'date-fns/add_minutes'
import * as dateCompareAsc from "date-fns/compare_asc";
import * as dateFormat from "date-fns/format";
import * as dateAddMonths from "date-fns/add_months";
import {RefreshToken} from "../entity/RefreshToken";

class AuthController {

    async register(request: Request, response: Response) {
        let user = new User();
        //Получаем переданные пользователем данные
        user.firstName = request.body.firstName;
        user.lastName = request.body.lastName;
        user.username = request.body.username;
        user.password = request.body.password;
        user.email = request.body.email;
        // TypeORM валидация
        validate(user).then(errors => {
                if (errors.length > 0) {
                    return response.status(409).json(errors);
                } else {
                    // console.log("validation succeed");
                }
            }
        );

        const userRepository = getRepository(User);
        //Проверяем, нет ли пользователя с указанным электронным адресом или никнеймом в базе
        if (
            await userRepository.findOne({email: request.body.email})
        ) {
            response.status(404).json('User with this email exists');
            return;
        } else if (
            await userRepository.findOne({username: request.body.username})
        ) {
            response.status(404).json('User with this username exists');
            return;
        } else {
            user.hashPassword();
            // создаём токен
            // const token = jwt.sign(
            //     {userId: user.id, username: user.username},
            //     config.ts.jwtSecret,
            //     {expiresIn: "1h"}
            // );
            // user.token = token;
            // добавляем нового пользователя в базу
            await userRepository.save(user);
            response.status(200).json(user);

        }
    }

   public async login(request: Request, response: Response) {

        //Проверяем
        const {username, password} = request.body;

        if (!(username && password)) {
            response.status(400).send();
            return;
        }

        //Получаем пользователя по введённому никнейму
        const userRepository = getRepository(User);
        let user;
        try {
            user = await userRepository.findOneOrFail({where: {username}});
        } catch (error) {
            response.status(401).json('Incorrect user');
            return;
        }

        //Проверяем, корректный ли пароль
        if (!user.checkIfUnencryptedPasswordIsValid(password)) {
            response.status(402).send();
            return;
        }

        //Устанавливаем access token, refresh token
        const accessToken = this.generateJwt(user);
        // const accessToken = jwt.sign(
        //     {userId: user.id, username: user.username},
        //     options.jwtOpts.secret,
        //     {expiresIn: "1h"}
        // );

        const refreshToken =
            {
                user: user.id,
                refreshToken: new rand(/[a-zA-Z0-9_-]{64,64}/).gen(),
                expiration: dateAddMonths(new Date(), 1),
                isValid: true
            };

        // сохраняем refresh token в базу
        const newRefresh = new RefreshToken();
        newRefresh.user = refreshToken.user;
        newRefresh.refresh_token = refreshToken.refreshToken;
        newRefresh.expiration = refreshToken.expiration;
        newRefresh.isValid = refreshToken.isValid;

        let refreshTokenRepository = getRepository(RefreshToken);
        try {
            await refreshTokenRepository.save(newRefresh);
        } catch (err) {
            return response.status(400).json('error');
        }
        // И отправляем ответ
        response.status(200);
       return response.json({user: user, accessToken: accessToken, refreshToken: refreshToken});
    };

    // Когда закончился срок действия access token`а, нам необхожимо получить новый с помощью refresh token`а
    async refreshAccessToken(request: Request, response: Response) {
        const {user, token} = request.body;

        if (!user || !token) {
            return response.status(400).send('Invalid refresh or user')
        }

        let refreshTokenRepository = getRepository(RefreshToken);

        const userRefreshToken = await refreshTokenRepository.findOneOrFail({
            where: {
                refresh_token: token.refreshToken
            }, relations: ['user']
        });

        if (userRefreshToken.user.id !== user.id) {
            return response.status(400);
        }
        // Проверяем, не истёк ли срок действия токена
        const refreshTokenIsValid = dateCompareAsc(
            dateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
            dateFormat(userRefreshToken.expiration, 'YYYY-MM-DD HH:mm:ss'),
        );

        if (refreshTokenIsValid !== -1) {
            return response.status(400).json('Refresh token is expired');
        }

        // старый токен делаем невалидным
        userRefreshToken.isValid = false;
        try {
            await refreshTokenRepository.save(userRefreshToken);
        } catch (error) {
            response.status(401);
            return;
        }

        // создаём новую пару токенов
        const accessToken = this.generateJwt(user);
        // const accessToken = jwt.sign(
        //     {userId: user.id, username: user.username},
        //     options.jwtOpts.secret,
        //     {expiresIn: "1h"}
        // );
        const refreshToken =
            {
                user: user.id,
                refreshToken: new rand(/[a-zA-Z0-9_-]{64,64}/).gen(),
                expiration: dateAddMonths(new Date(), 1),
                isValid: true
            };

        // сохраняем refresh token в базу
        const newRefresh = new RefreshToken();
        newRefresh.user = refreshToken.user;
        newRefresh.refresh_token = refreshToken.refreshToken;
        newRefresh.expiration = refreshToken.expiration;
        newRefresh.isValid = refreshToken.isValid;
        try {
            await refreshTokenRepository.save(newRefresh);
        } catch (err) {
            return response.status(400).json('error');

        }
        // И отправляем ответ
        response.status(200);
        response.json({user: user, accessToken: accessToken, refreshToken: refreshToken});

    };


    async forgot(request: Request, response: Response) {
        const req = request.body;

        if (!req.email) {
            response.status(402).json();
            return;
        }


        // создаём токен и срок его действия для смены пароля
        let resetData = {
            passwordResetToken: new rand(/[a-zA-Z0-9_-]{64,64}/).gen(),
            passwordResetExpiration: dateAddMinutes(new Date(), 30),
        };

        const userRepository = getRepository(User);
        let user: User;

        // привязываем токен пользователю, который хочет сменить пароль
        try {
            user = await userRepository.findOneOrFail({where: {email: req.email}});
            user.passwordResetExpiration = resetData.passwordResetExpiration;
            user.passwordResetToken = resetData.passwordResetToken;

            await userRepository.save(user);
            response.status(200).json({
                passwordResetToken: resetData.passwordResetToken,
                passwordResetExpiration: resetData.passwordResetExpiration
            });
        } catch (error) {
            response.status(400).send('user nor found');
            return;
        }

        // Далее формируем данные , которые будем отправлять пользователюна почту для сброса пароля

        // if (request.type === 'web') {
        //     let email = await fse.readFile('./src/email/forgot.html', 'utf8')
        //     let resetUrlCustom =
        //         request.url +
        //         '?passwordResetToken=' +
        //         resetData.passwordResetToken +
        //         '&email=' +
        //         request.email
        //
        //     const emailData = {
        //         to: request.email,
        //         from: process.env.APP_EMAIL,
        //         subject: 'Password Reset For ' + process.env.APP_NAME,
        //         html: email,
        //         categories: ['koa-vue-notes-api-forgot'],
        //         substitutions: {
        //             appName: process.env.APP_NAME,
        //             email: request.email,
        //             resetUrl: resetUrlCustom,
        //         },
        //     }
        //
        //
        //     if (process.env.NODE_ENV !== 'testing') {
        //         await sgMail.send(emailData)
        //     }
        // }

    }


    async check(request: Request, response: Response) {
        const req = request.body;
        if (!req.passwordResetToken) {

            response.status(405).json('Incorrect data');
            return;
        }
        const userRepository = getRepository(User);
        let user;
        try {
            user = await userRepository.findOneOrFail({
                where: {
                    email: req.email,
                    passwordResetToken: req.passwordResetToken
                }
            })
        } catch (error) {

            response.status(401).json('user nor found');
            return;
        }

        if (!user.passwordResetExpiration) {

            response.status(402).json('Invalid token');
            return;
        }

        //Let's make sure the refreshToken is not expired
        let tokenIsValid = dateCompareAsc(
            dateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
            dateFormat(user.passwordResetExpiration, 'YYYY-MM-DD HH:mm:ss'),
        );
        if (tokenIsValid !== -1) {
            response.status(403).json('Invalid data');
            return;
        }
        response.status(200).json({message: 'success'});
    }

    async reset(request: Request, response: Response) {
        const req = request.body;

        let userData = new User();
        userData.email = request.body;
        userData.passwordResetToken = request.body;
        userData.password = request.body.password;
        const userRepository = getRepository(User);
        let user;
        try {
            user = await userRepository.findOneOrFail({where: {email: request.body.email}})
        } catch (error) {
            response.status(400).send('user nor found');
            return;
        }

        if (!user.passwordResetExpiration) {
            response.status(401).json('Incorrect data');
            return;
        }

        let tokenIsValid = dateCompareAsc(
            dateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
            dateFormat(user.passwordResetExpiration, 'YYYY-MM-DD HH:mm:ss')
        );
        if (tokenIsValid !== -1) {
            response.status(402).json('Incorrect data');
            return;
        }
        //убираем из базы токен
        userData.passwordResetToken = null;
        userData.passwordResetExpiration = null;
        user.passwordResetToken = userData.passwordResetToken;
        user.passwordResetExpiration = userData.passwordResetExpiration;
        user.password = userData.password;
        user.hashPassword();
        try {
            await userRepository.save(user);
            response.status(200).json(user);
        } catch (err) {

            response.status(408).json(err);
            return;
        }

    }

    private async generateJwt(user) {
        const accessToken = jwt.sign(
            {userId: user.id, username: user.username},
            options.jwtOpts.secret,
            {expiresIn: "1h"}
        );
        return accessToken;
    }

}

export default new AuthController()
