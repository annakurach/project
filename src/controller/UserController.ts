import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {User} from "../entity/User";
import {validate} from "class-validator";
export class UserController {

    static all = async(request: Request, response: Response) => {
        let users = await getRepository(User).find();
        response.send(users);
    }

    static one = async(request: Request, response: Response) =>{
        const errors = await validate(request.body);
        if (errors.length > 0) {
            response.status(400).send(errors);
            return;
        }
        let user;
        try {
            user = await getRepository(User).findOneOrFail(request.params.id);
            response.send(user);
        } catch (error) {
            return response.status(404).json('user not found');
        }
    }

    static save = async(request: Request, response: Response) => {
        let user = new User();
        user.firstName = request.body.firstName;
        user.lastName = request.body.lastName;
        user.username = request.body.username;
        user.password = request.body.password;
        user.email = request.body.email;
        validate(user).then(errors => {
                if (errors.length > 0) {
                    console.log(errors);
                    return response.status(409).json(errors);
                } else {
                    console.log("validation succeed");
                }
            }
        );
        const userRepository = getRepository(User);
        try {
            return await userRepository.save(user);
        } catch (e) {
            response.status(408).send("username already in use");
            return;
        }
    }

    static edit = async (req: Request, res: Response) => {
        const id = req.params.id;
        const { firstName, lastName, username, password, email} = req.body;
        const userRepository = getRepository(User);
        let user;
        try {
            user = await userRepository.findOneOrFail(id);
        } catch (error) {
            res.status(404).send("User not found");
            return;
        }
        user.firstName = firstName;
        user.lastName = lastName;
        user.username = username;
        user.password = password;
        user.email = email;
        const errors = await validate(user);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }
        let newUser;
        try {
            newUser = await userRepository.save(user);
        } catch (e) {
            res.status(409).send("error");
            return;
        }
        res.status(200).send(newUser);
    };

    static remove = async(request: Request, response: Response) => {
        //Get the ID from the url
        const id = request.params.id;

        const userRepository = getRepository(User);
        let user: User;
        try {
            user = await userRepository.findOneOrFail(id);
        } catch (error) {
            response.status(404).send("User not found");
            return;
        }
        await userRepository.delete(id);

        //After all send a 204 (no content, but accepted) response
        response.status(204).send();
    }

    static deleteAll = async(request: Request, response: Response) => {
        const userRepository = getRepository(User);
        await userRepository.clear();
    }


}

export default UserController;
