import * as dotenv from "dotenv";
import {ConnectionOptions} from "typeorm";
import {join} from "path";
if (process.env.NODE_ENV) {
    process.env.NODE_ENV
        .split(',')
        .filter(val => val)
        .forEach(envPath => {
            dotenv.config({path: __dirname + "/../../.env."+envPath});
        })
}
const parentDir = join(__dirname, '..');
const connectionOpts: ConnectionOptions = {
    type: 'postgres',
    host: process.env.TYPEORM_HOST ,
    port: Number(process.env.TYPEORM_PORT) ,
    username: process.env.TYPEORM_USERNAME ,
    password: process.env.TYPEORM_PASSWORD ,
    database: process.env.TYPEORM_DATABASE ,
    entities: [
        `${parentDir}/entity/*.ts`,
    ],
    synchronize: true,
};

const jwtOpts = {
    secret: process.env.JWT_SECRET
};

const serverOpts = {
    port: process.env.SERVER_PORT || 3000
}
const options = {
    connectionOpts : connectionOpts,
    jwtOpts: jwtOpts,
    serverOpts: serverOpts
};
console.log(options);
export default options;

